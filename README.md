# Orientation in Field



## Project Explanation

RoboCup Humanoid League Kid Size is an international soccer competition of humanoid robots. As the field is symmetric, you can't know which side you are on based on the field features alone. This work contributes by proposing an algorithm to estimate the robot's orientation based on the background. To do it, we use a feature detector to match the features between the image and reference images. Then, as we know the reference images' orientation, we can calculate the current orientation based on their difference. We searched for parameters to optimize the algorithm. Using the parameters found, we can run the algorithm on 101 ms on average, while having a mean absolute error of 6.9º.

## Files

- ```orientation_finder.py```: Class that performs the algorithm;
- ```params.py```: Class that handles the parameters;
- ```tester.py```: Runs the algorithm on the dataset;
- ```utils.py```: Misc. helper functions;
- ```random_search.py```: Random search for good parameters.

## Dataset

[Click here to download the dataset.](https://drive.google.com/file/d/1zgNdY7ziWyh9EMcQzAxUZLhvVdhBi4r2/view?usp=share_link)
